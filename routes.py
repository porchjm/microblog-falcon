import json
import falcon
from data import Author, Item

class author(object):
    def on_get(self, req, resp, aid):
        auth = Author.get(aid)
        if auth is None:
            resp.body = json.dumps({"Error":"Author not found"})
            resp.status=falcon.HTTP_404
            return
        resp.body = json.dumps(auth)
        resp.status=falcon.HTTP_200

class authors(object):
    def on_get(self, req, resp):
        auth = Author.get_all()
        resp.body = json.dumps(auth)
        resp.status=falcon.HTTP_200

class item(object):
    def on_get(self, req, resp, uid):
        itm = Item.get(uid)
        if itm is None:
            resp.body = json.dumps({"Error":"Post not found"})
            resp.status=falcon.HTTP_404
            return
        resp.body = json.dumps(itm)
        resp.status=falcon.HTTP_200

class items(object):
    def on_get(self, req, resp):
        itm = Item.get_all()
        resp.body = json.dumps(itm)
        resp.status=falcon.HTTP_200