# microblog-falcon

Microblog demo implemented in Falcon

Currently uses SQLite as a backing datastore. Run test_setup.py to populate a demo database.

You'll need falcon installed for the basic system, and it's currently using waitress for the test server.