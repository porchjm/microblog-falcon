import falcon
import json
import data
import routes

api = application = falcon.API()

api.add_route('/items', routes.items())
api.add_route('/items/{uid}',routes.item())
api.add_route('/authors', routes.authors())
api.add_route('/authors/{aid}',routes.author())