import json
import sqlite3


author_data = {
    "1" : {
        "name":"Jesse M. Porch",
        "url":"https://jmporch.com",
        "avatar":""
    },
    "2" : {
        "name":"JayPoe",
        "url":"https://jmporch.com",
        "avatar":""
    }
}

post_data = {
    "100001": {
        "url":"https://jmporch.com/items/100001",
        "external_url":"",
        "title":"Post 1",
        "content_html":"<p>Content 1</p>",
        "content_text":"Content 1",
        "summary":"1",
        "image":"",
        "banner_image":"",
        "date_published":"",
        "date_modified":"",
        "author":"1",
        "tags":"Test1",
        "attachments":""
    },
    "100002": {
        "url":"https://jmporch.com/items/100002",
        "external_url":"",
        "title":"Post 2",
        "content_html":"<p>Content 2</p>",
        "content_text":"Content 2",
        "summary":"2",
        "image":"",
        "banner_image":"",
        "date_published":"",
        "date_modified":"",
        "author":"2",
        "tags":"Test1,Test2",
        "attachments":""
    }
}

conn = sqlite3.connect("data.db")
c = conn.cursor()

sql = "DROP TABLE IF EXISTS authors;"
c.execute(sql)
sql = "CREATE TABLE authors(id string PRIMARY KEY, name string, url string, avatar string);"
c.execute(sql)

sql = "DROP TABLE IF EXISTS items;"
c.execute(sql)
sql = "CREATE TABLE items(id string PRIMARY KEY, url string, external_url string, title string, content_html string, content_text string, summary string, image string, banner_image string, date_published string, date_modified string, author string, tags string, attachments string);"
c.execute(sql)

conn.commit()

for key, ath in author_data.items():
    sql = "INSERT INTO authors VALUES (?,?,?,?)"
    c.execute(sql,(key,ath["name"],ath["url"],ath["avatar"]))

conn.commit()


for key, itm in post_data.items():
    sql = "INSERT INTO items VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
    c.execute(sql,(key,itm["url"],itm["external_url"],itm["title"],itm["content_html"],itm["content_text"],itm["summary"],itm["image"],itm["banner_image"],itm["date_published"],itm["date_modified"],itm["author"],itm["tags"],itm["attachments"]))

conn.commit()

c.execute("SELECT * FROM authors")
for i in c.fetchall():
    print(i)

c.execute("SELECT * FROM items")
for i in c.fetchall():
    print(i)