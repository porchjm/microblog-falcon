import json
import sqlite3

class Author(object):
    @staticmethod
    def get(id):
        sql = "SELECT * FROM authors WHERE id = ?"
        conn = sqlite3.connect("data.db")
        c = conn.cursor()
        i = c.execute(sql,(id,)).fetchone()
        if i is None:
            return i
        return {
            "id": i[0],
            "name": i[1],
            "url": i[2],
            "avatar": i[3]
        }

    @staticmethod
    def get_all():
        sql = "SELECT * FROM authors"
        conn = sqlite3.connect("data.db")
        c = conn.cursor()
        data = []
        for i in c.execute(sql).fetchall():
            item = {
                "id": i[0],
                "name": i[1],
                "url": i[2],
                "avatar": i[3]
            }
            data.append(item)
        return data

class Item(object):
    @staticmethod
    def get(id):
        sql = "SELECT * FROM items WHERE id = ?"
        conn = sqlite3.connect("data.db")
        c = conn.cursor()
        i = c.execute(sql,(id,)).fetchone()
        if i is None:
            return i
        return {
            "id": i[0],
            "url": i[1],
            "external_url": i[2],
            "title": i[3],
            "content_html": i[4],
            "content_text": i[5],
            "summary": i[6],
            "image": i[7],
            "banner_image": i[8],
            "date_published": i[9],
            "date_modified": i[10],
            "author": i[11],
            "tags" : i[12],
            "attachments" : i[13]
        }

    @staticmethod
    def get_all():
        sql = "SELECT * FROM items"
        conn = sqlite3.connect("data.db")
        c = conn.cursor()
        data = []
        for i in c.execute(sql).fetchall():
            item = {
                "id": i[0],
                "url": i[1],
                "external_url": i[2],
                "title": i[3],
                "content_html": i[4],
                "content_text": i[5],
                "summary": i[6],
                "image": i[7],
                "banner_image": i[8],
                "date_published": i[9],
                "date_modified": i[10],
                "author": i[11],
                "tags" : i[12],
                "attachments" : i[13]
            }
            data.append(item)
        return data
        #return {k: v for k, v in Item.data[id].items() if v != "" and v != [] and v != {}}